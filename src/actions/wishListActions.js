import { ADD_PHOTO } from './types';

// Add Photo to WishList Action
export const addToWishList = (photo) => dispatch => {
      dispatch({
        type: ADD_PHOTO,
        payload: photo
      })
};