import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

import { fetchAlbums } from '../actions/albumActions';
import { fetchPhotos, fetchPhoto } from '../actions/photoActions';
import { addToWishList } from '../actions/wishListActions';
import { connect } from 'react-redux';


import WishList from '../components/WishList';
import Albums from '../components/Albums';
import Photos from '../components/Photos';
import Photo from '../components/Photo';
import Error from '../components/Error';
import Home from "../components/Home";


// Inline Styles
const containerStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  marginRight: 'auto',
  marginLeft: 'auto',
  alignItems: 'flex-start',
};

const divStyle = {
  display: 'flex',
  flexDirection: 'wrap',
  boxShadow: '0 15px 30px 1px rgba(128, 128, 128, 0.31)',
	background: 'rgba(255, 255, 255, 0.90)',
	textAlign: 'center',
  borderRadius: '5px',
  alignItems: 'flex-start'
	
};

//---------------------------------------------
//  Main Screen
//---------------------------------------------
 class App extends React.Component {

    componentWillMount() {
        this.props.fetchAlbums();
      }

    render(){
        return (
                  <BrowserRouter >
                  <div style={containerStyle}>
                      <Albums style={divStyle} count={this.props.wishListCount}  albums={this.props.albums}/>
                      <Switch >
                            <Route path="/"  component={Home} exact />
                            <Route path="/photos/:albumId" render={({match})=><Photos  style={divStyle}   photos={this.props.photos} albumId={match.params.albumId} getPhotos={this.props.fetchPhotos} addtowishlist={this.props.addToWishList}  /> } />
                            <Route path="/photo/:photoId" render={({match})=><Photo style={divStyle}  photo={this.props.photo} photoId={match.params.photoId} getPhoto={this.props.fetchPhoto} addtowishlist={this.props.addToWishList}  /> } />
                            <Route path="/wishlist" render={()=> <WishList style={divStyle}  wishlist={this.props.wishlist} /> } />
                            <Route component={Error} />
                      </Switch>
                 </div>
                </BrowserRouter>
          );
    }
};

//Defining PropTypes for App
App.propTypes = {
    fetchAlbums: PropTypes.func.isRequired,
    fetchPhotos: PropTypes.func.isRequired,
    fetchPhoto: PropTypes.func.isRequired,
    addToWishList: PropTypes.func.isRequired,
    albums: PropTypes.array.isRequired,
    photos: PropTypes.array.isRequired,
    wishlist: PropTypes.array.isRequired,
    wishListCount: PropTypes.number.isRequired
  };

//Defining How We Map Store State to App Props
const mapStateToProps  = state => ({
        albums: state.albumReducer.albums,
        photos: state.photoReducer.photos,
        photo: state.photoReducer.photo,
        wishlist: state.wishListReducer.wishlist,
        wishListCount: state.wishListReducer.count
});

//  link App with the store state And Expoting it
export default connect( mapStateToProps ,  
                        { fetchAlbums, fetchPhotos, addToWishList, fetchPhoto })(App);