import React from 'react'
import { NavLink } from 'react-router-dom'; 

// Inline Styles
const divStyle = {
  width: '15%',
 borderRight: '1px solid #000',
 textAlign: 'center',
 marginBottom: '1em',
 marginRight: '1%',
 backgroundColor: "#333",
 color: '#333'
};

const link = {
  width: '100%',
 color: "#FFF",
 textDecoration: "none",
 display: "inline-block",
 borderBottom: '1px solid #bbb'
};

//---------------------------------------------
//  Albums Page
//---------------------------------------------
const  Albums = (props) => {
    const albums = props.albums.map(element => (
        <NavLink style={link} key={element.id}  activeStyle={{ backgroundColor: '#4CAF50' }} 
         to={`/photos/${element.id}`} >
            <h6>{element.title.toUpperCase()}</h6>
         </NavLink>
    ));
  return (
    <div style={divStyle}>
        <NavLink style={link}   activeStyle={{ backgroundColor: '#4CAF50' }}  to="/wishlist" >
            <h3> WISHLIST {props.count}</h3>
        </NavLink>
        <span style={link}><h3>Oyez Albums</h3></span>
          {albums}
    </div>
  )

}

export default  Albums;