import React from 'react';

// Inline Styles
const divStyle = {
  padding: '5px',
  margin: '10%',
  textAlign: 'center',
  color: 'red',
};

//---------------------------------------------
//  Error || Not Found Page
//---------------------------------------------
const Error = () => {
  return (
    <div style={divStyle}>
      <h1>Are you lost !!!!!</h1>
    </div>
  );
};

export default Error;