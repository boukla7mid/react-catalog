import React from 'react';
import { NavLink } from 'react-router-dom';

// Inline Styles
const containerStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  width: '83%',
  alignItems: 'flex-start'
};

const divStyle = {
  display: 'flex',
  flexDirection: 'column',
  padding: '5px',
  margin: '5px',
  boxShadow: '0 15px 30px 1px rgba(128, 128, 128, 0.31)',
	background: 'rgba(255, 255, 255, 0.90)',
	textAlign: 'center',
  borderRadius: '5px',
  
};

const imgStyle = {
  height: '250px',
  width: '250px'
};

//---------------------------------------------
//  Simple Button with some styles
//---------------------------------------------
const WishList =  (props) => {
  const photos = props.wishlist.map(element => (
    <div style={divStyle} key={element.id}>
      <NavLink to={`/photo/${element.id}`} ><img src={element.url} style={imgStyle} ></img></NavLink>
    </div>
    ));

  return (
    <div style={containerStyle}>
      {photos}
    </div>
  )
};

export default WishList;
