import React from 'react';

// Inline Styles
const btnStyle = {
    backgroundColor: '#4CAF50', 
    border: 'none',
    borderRadius: '2px',
    color: 'white',
    padding: '15px 32px',
    textAlign: 'center',
    textDecoration: 'none',
    display: 'inline-block',
    fontSize: '16px',
    width: '100%',
    cursor: 'pointer'
  };
//---------------------------------------------
//  Simple Button with some styles
//---------------------------------------------
const Button =  (props) => {
  return (
    <div>
      <button style={btnStyle} onClick={(e) => props.addtowishlist(props.photo)}>Add To WishList</button>
    </div>
  )
};

export default Button;
