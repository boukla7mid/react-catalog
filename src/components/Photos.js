import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'; 
import Button from './Button';

// Inline Styles
const containerStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  width: '83%',
  alignItems: 'flex-start'
};

const divStyle = {
  display: 'flex',
  flexDirection: 'column',
  padding: '5px',
  margin: '5px',
  boxShadow: '0 15px 30px 1px rgba(128, 128, 128, 0.31)',
	background: 'rgba(255, 255, 255, 0.90)',
	textAlign: 'center',
  borderRadius: '5px',
  
	
};

const imgStyle = {
  height: '250px',
  width: '250px'
};

//---------------------------------------------
//  Album's Photos Page
//---------------------------------------------

 class Photos extends Component {

    constructor(props){
      super(props);
    }

    componentWillReceiveProps(nextProps) {
        const currentId = this.props.albumId;
        const nextId = nextProps.albumId;
        if (currentId !== nextId) {
          nextProps.getPhotos(nextId);
        }    
    }

    componentDidMount() {
        this.props.getPhotos(this.props.albumId);
    }
    
    render() {
        const photos = this.props.photos.map(element => (
            <div style={divStyle} key={element.id}>
              <NavLink to={`/photo/${element.id}`} ><img src={element.url} style={imgStyle} ></img></NavLink>
              <Button  addtowishlist={this.props.addtowishlist} photo={element} />
            </div>
        ));
        return (
          <div style={containerStyle}>
            {photos}
          </div>
        )
    }
}

export default Photos;