import React, { Component } from 'react';
import Button from './Button';

// Inline Styles
const divStyle = {
  display: 'flex',
  flexDirection: 'column',
  padding: '15px',
  marginRight: 'auto',
  marginLeft: 'auto'
};

//---------------------------------------------
//  Individual Photo Page
//---------------------------------------------
class Photo extends Component {

    constructor(props){
      super();
    }

    componentWillMount(){
      this.props.getPhoto(this.props.photoId);
    }
    render() {
      return (
        <div style={divStyle}>
          <div style={divStyle} key={this.props.photo.id}>
              <img src={this.props.photo.url}></img>
              <p>{this.props.photo.title}</p>
              <Button  addtowishlist={this.props.addtowishlist} photo={this.props.photo} />
          </div>
      </div>
      )
    }
}

export default  Photo;