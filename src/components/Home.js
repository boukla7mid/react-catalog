import React from 'react'

const divStyle = {
    padding: '5px',
    display: 'flex',
    flexDirection: 'column',
    margin: '15%',
    textAlign: 'center',
    color: '#4CAF50',
  };

const Home = () => {
  return (
    <div style={divStyle}>
        <h1>Welcome to Oyezt CATALOG</h1>
        <ul>
          <li>Click At Albums'names, at right, to navigate from an album to another</li>
          <li>Click At WishList to view your wished photos</li>
        </ul>
    </div>
  )
}

export default Home;